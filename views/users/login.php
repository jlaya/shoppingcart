<!DOCTYPE html>
 <html>
 <head>
 	<title>Login</title>
 	<!-- CDN CSS and JS -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="../../assets/css/bootstrap.4.4.1.min.css">
	<link rel="stylesheet" href="../../assets/css/style.css">
	<style type="text/css">
		a{
			color:#000000;
			text-decoration: none;
		}

		a:hover{
			color:#FFFFFF;
			text-decoration: none;
		}

	</style>

	<!-- jQuery library -->
	<script src="../../assets/js/jquery.3.4.1.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="../../assets/js/bootstrap.4.4.1.min.js"></script>
 </head>
 <body class="text-center">
 	<!-- View menu -->
	<?php require_once("../../views/menu.php");  ?>

	<p>
		<div class="container">
      		<div class="row">
      			<div class="col-12">
      				<form class="form-signin" action="../../controllers/FrontController.php?action=login" method="POST">
      				  <img class="mb-4" src="../../assets/image/shop_cart.png" alt="" width="72" height="72">
				      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
				      <label for="email" class="sr-only">Email address</label>
				      <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus>
				      <label for="inputPassword" class="sr-only">Password</label>
				      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
				      
				      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
				      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
				    </form>
				</div>
			</div>
		</div>
	</p>
 	
 </body>
 </html>