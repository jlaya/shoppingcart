<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <a class="navbar-brand" href=""><?php echo $_SESSION['email']; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="">Home <span class="sr-only">(current)</span></a>
      </li>
      <?php if(isset($_SESSION['email'])){ ?>
        <?php if($_SESSION['email'] != "admin@gmail.com"){ ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#DetailCartModal">
              List Cart
            </a>
          </li>
          <li class="nav-item">
            <button type="button" class="btn btn-primary">
              To cart <span class="badge badge-light count_cart"></span>
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn btn-warning">
              <span class="badge badge-light disponibility"></span>
            </button>
          </li>
        <?php } ?>
      <?php } ?>
      <?php if($_SESSION['email'] == "admin@gmail.com"){ ?>
        <li class="nav-item">
          <button type="button" class="btn btn-warning average">
            Average
          </button>
        </li>
      <?php } ?>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <?php if($_SESSION['email'] == ""){ ?>
        <a href="views/users/login.php">
          Sign in
        </a>
      <?php }else{ ?>
        <a href="../../shoppingcart/controllers/FrontController.php?action=session_destroy">
          Sign out
        </a>
      <?php } ?>
    </form>
  </div>
</nav>