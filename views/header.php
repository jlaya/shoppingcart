<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.4.4.1.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<style type="text/css">
	a{
		color:#000000;
		text-decoration: none;
	}

	a:hover{
		color:#FFFFFF;
		text-decoration: none;
	}

</style>

<!-- jQuery library -->
<script src="assets/js/jquery.3.4.1.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="assets/js/bootstrap.4.4.1.min.js"></script>