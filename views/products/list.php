
<?php 
	
	# Load up session
	session_start();

 ?>

<div class="container">
	<div class="row" style="margin-top: 60px;">

	<?php foreach ($list_products as $key => $value) {  ?>
	  			<div class="card border border-primary col-3">
	  				<strong class="card-title"><?php echo $value['name']; ?></strong>
	  				<strong class="card-amount"><?php echo $value['amount']; ?>$</strong>
	  				<br>
	  				<img src="<?php echo $value['avatar']; ?>" style="width: 100%;height: 180px;"/>

	  				<p class="clasificacion">
						<input class="star" id="radio5" type="checkbox" name="start" value="<?php echo $value['id']; ?>;5">
						<label for="radio5">&#9733;</label>
						<input class="star" id="radio4" type="checkbox" name="star" value="<?php echo $value['id']; ?>;4">
						<label for="radio4">&#9733;</label>
						<input class="star" id="radio3" type="checkbox" name="star" value="<?php echo $value['id']; ?>;3">
						<label for="radio3">&#9733;</label>
						<input class="star" id="radio2" type="checkbox" name="star" value="<?php echo $value['id']; ?>;2">
						<label for="radio2">&#9733;</label>
						<input class="star" id="radio1" type="checkbox" name="star" value="<?php echo $value['id']; ?>;1">
						<label for="radio1">&#9733;</label>
					</p>

					<button
					 data-id="<?php echo $value['id']; ?>"
					 data-name="<?php echo $value['name']; ?>"
					 data-avatar="<?php echo $value['avatar']; ?>"
					 data-amount="<?php echo $value['amount']; ?>"
					 class="btn btn-primary get-data" type="button" data-toggle="modal" data-target="#exampleModal">
					More details
					</button><br>
				</div>
	<?php } ?>
	</div>
</div>


	<!-- Modal Add Cart -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="product-name"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="container">
	      		<div class="row">
	      			<div class="col-12">
	      				<div class="avatar"></div>
	      				Price: <span id="product-amount"></span>$
					</div>
				</div>
			</div>
			<?php if($_SESSION['email'] != ""){ ?>
		        <div class="container">
				  <div class="row">
				  	<div class="col-6">
				  		<label for="quantity">Quantity</label>
				  		<input type="hidden" name="id" id="id">
				  		<input type="hidden" name="name" id="name">
				  		<input type="hidden" name="amount" id="amount">
				  		<input type="number" name="quantity" id="quantity">
				  	</div>
				  	<div class="col-6">
				  		<br>
				  		<button class="btn btn-primary add-cart">Add cart</button>
				  	</div>
				  </div>
				</div>
			<?php } ?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal Details list up shoping cart online -->
	<div class="modal fade" id="DetailCartModal" tabindex="-1" role="dialog" aria-labelledby="DetailCartModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Details list up shoping cart online</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="container">
			  <div class="row">
			  	<div class="col-12">
			  		<div class="content-listcart"></div>
			  	</div>
			  	<?php if($_SESSION['email'] != ""){ ?>
				  	<div class="col-12" style="margin-top: 25px;">
				  		<h5>
				  			<label>Shipping option</label>
				  			<select title="To Shipping" class="form-control" id="shipping">
				  				<option value="0">-------------</option>
				  				<option value="pick_up">Pick up</option>
				  				<option value="ups">UPS</option>
				  			</select>
				  		</h5>
				  	</div>
				  	<div class="col-12" style="margin-top: 25px;">
				  		<button type="button" class="btn btn-secondary pay">Pay</button>
				  	</div>
			  	<?php } ?>
			  </div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal Info detail balance -->
	<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="container">
	      		<div class="row">
	      			<div class="col-12">
	      				<p>
	      					<strong>Previous balance</strong>: <span id="previous_balance"></span>$
	      				</p>
	      				<p>
	      					<strong>Cost total</strong>: <span id="total_cost"></span>$
	      				</p>
	      				<p>
	      					<strong>Remaining balance</strong>: <span id="remaining_balance"></span>$
	      				</p>
					</div>
				</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal Info average -->
	<div class="modal fade" id="averageModal" tabindex="-1" role="dialog" aria-labelledby="averageModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Average</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="container">
	      		<div class="row">
	      			<div class="col-12">
	      				<div class="table-average"></div>
					</div>
				</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>


<!-- Load  for  events jquery -->
<script type="text/javascript">
	
	$( document ).ready(function() {
	    
	    // get for informations the products and view print result
	    $("button.get-data").click(function(){
		  var id = $(this).data('id');
		  var name = $(this).data('name');
		  var avatar = $(this).data('avatar');
		  var amount = $(this).data('amount');
		  var avatar_url = "<img src="+avatar+" >";

		  $("#id").val(id);
		  $("#name").val(name);
		  $("div.avatar").html(avatar_url);
		  $("#amount").val(amount);
		  $("h5#product-name").text(name);
		  $("span#product-amount").text(amount);
		});

		// Disponibility users
		disponibility = function(){

			// Action load Ajax
			$.ajax({
		    
			    type: "POST",
			    
			    dataType: "html",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=users_disponibility",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			         result = data;
			         $("span.disponibility").html(result);
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});

		}

	    // List cart products session data
		listcart = function(){

			// Action load Ajax
			$.ajax({
		    
			    type: "POST",
			    
			    dataType: "html",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=listcart",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			         result = data;
			         $("div.content-listcart").html(result);
			         var count_refresh = $("span.count_refresh").text();
			         $("span.count_cart").html(count_refresh);
			         disponibility();
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});

		}

		listcart();

		// Remove value unset session array
		remove_session_cart = function( id ){

			$.ajax({
		    
			    type: "GET",
			    
			    dataType: "json",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=removecart&id="+id,
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			     	count = data.count;
		         	$("span.count_cart").text(count);
			        listcart();
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});

		}


		// Add cart  data online product
	    $("button.add-cart").click(function(){
		  var product_id = $("#id").val();
		  var name = $("#name").val();
		  var quantity = $("#quantity").val();
		  var amount = $("#amount").val();

		  if(quantity == "" || quantity == 0){

		  	alert("Sorry, field required");
		  	return true;

		  }
		  
		  result = quantity * amount;

		  // Action load Ajax
		  $.ajax({
		    
		    data: {
		    	"name" : name, 
		    	"product_id" : product_id, 
		    	"quantity" : quantity,
		    	"amount" : amount,
		    	"result" : result
		    },
		    
		    type: "POST",
		    
		    dataType: "json",
		    
		    url: "../../shoppingcart/controllers/FrontController.php?action=addcart",
		})
		 .done(function( data, textStatus, jqXHR ) {
		     if ( console && console.log ) {

		     	 $("#quantity").val("");
		     	 $('#exampleModal').modal('hide');

		         count = data.count;
		         $("span.count_cart").text(count);
		         listcart();
		     }
		 })
		 .fail(function( jqXHR, textStatus, errorThrown ) {
		     if ( console && console.log ) {
		         console.log( "Load Failed: " +  textStatus);
		     }
		});
		  

		});

	    // Send to pay cart products
		$("button.pay").click(function(){
			var shipping = $("#shipping").val();
			if(shipping ==0){
				alert("Please, you must choose an option");
				return true;
			}

			// Action load Ajax
			$.ajax({
			    
			    data: {
			    	"shipping" : shipping
			    },
			    
			    type: "POST",
			    
			    dataType: "json",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=pay",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {

			     	 $('#DetailCartModal').modal('hide');
			         listcart();
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});





		});

		// Remove value unset session array
		history_balance = function(){

			$.ajax({
		    
			    type: "POST",
			    
			    dataType: "json",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=history_balance",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			     	previous_balance  = data[0]['previous_balance'];
			     	total_cost        = data[0]['total_cost'];
			     	remaining_balance = data[0]['remaining_balance'];
		         	$("span#previous_balance").text(previous_balance);
		         	$("span#total_cost").text(total_cost);
		         	$("span#remaining_balance").text(remaining_balance);
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});

		}

		// Info detail balance
		$("span.disponibility").click(function(){
			$('#detailModal').modal('show');
			history_balance();
		});

		// Info average
		$("button.average").click(function(){

			$.ajax({

			    type: "POST",
			    
			    dataType: "html",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=average",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			     	$("div.table-average").html(data);
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});
			
			$('#averageModal').modal('show');


			
		});

		// Puntuations for the stars  to user log in
		$("input.star").click(function(){
			star = $(this).val();
			
			$.ajax({
		    	
		    	data: {
			    	"star" : star
			    },

			    type: "POST",
			    
			    dataType: "html",
			    
			    url: "../../shoppingcart/controllers/FrontController.php?action=puntuations_star",
			})
			 .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			     	alert(data);
			     }
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "Load Failed: " +  textStatus);
			     }
			});


		});

		

	});



</script>