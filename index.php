<?php 
	
	# Conection for DB
	require_once("conn/index.php");
	require_once("controllers/CProducts.php");

	# Instance for conection DB
	$conn = new Conn();
	# Instance for call the controllers and methods
	$products = new CProducts();
	$list_products = $products->listProducts();

	# Load up session
	session_start();

	$count_cart = $_SESSION['count_cart'];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>ShopCart</title>
	<!-- CDN CSS and JS -->
	<?php require_once("views/header.php");  ?>
</head>
<body>

	<!-- View menu -->
	<?php require_once("views/menu.php");  ?>

	<!-- View slider -->
	<?php require_once("views/slider.php");  ?>

	<!-- View for list products -->
	<?php require_once("views/products/list.php");  ?>

</body>
</html>