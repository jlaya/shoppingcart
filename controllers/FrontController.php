<?php

// Global values sistem
require_once("../global/global.php");

# Variable GET for action url
$acc = $_GET['action'];

# Controllers
require_once("CUsers.php");

switch ($acc) {
    case "login":

    	# Load up session
		session_start();

    	# Param field post
    	$param = $_POST;
    	$email = $param['email'];
    	$password = hash('sha256', $param['password']);

    	$login = CUsers::login( $email , $password );

    	if( $login > 0 ){
    		$_SESSION['email'] = $email;
    		$header = "shoppingcart";
    	}else{
    		$header = "shoppingcart/views/users/login.php";
    	}

    	header( "Location:/" . $header);

        break;
    case "session_destroy":

    	# Load up session
		session_start();

    	// Session destroy
    	session_destroy();
    	header( "Location:/shoppingcart");

    	break;
    case "addcart":

    	# Load up session
		session_start();
		session_regenerate_id(true);

    	# Param field post
    	$param = $_POST;


    	$data = array(
    		"name" => $param['name'],
    		"product_id" => $param['product_id'],
    		"quantity" => $param['quantity'],
    		"amount" => $param['amount'],
    		"result" => $param['result']
    	);
    	
    	$_SESSION['cart'][] = $data;


		if (!isset($_SESSION['cart'])) {

			$_SESSION['count_cart'] = 0;

		} else {
			
			$_SESSION['count_cart'] = (int)$_SESSION['count_cart'] + 1;
		}


		/*for($i = 0 ; $i < count($_SESSION['cart']) ; $i++) {
			echo '<td>'.$_SESSION['cart'][$i]['name'].'</td>';
		}*/

		echo json_encode(array( 'count'=> $_SESSION['count_cart'] ));


        break;
    case "listcart":

    	# Load up session
		session_start();

		$table = "<table class='table' border=1 style='width:100%;'>";
		$table .= "<tr>";
			$table .= "<th>Product name</th>";
			$table .= "<th>Unit price</th>";
			$table .= "<th>Quantity</th>";
			$table .= "<th>Total price</th>";
			$table .= "<th>Remove</th>";
		$table .= "</tr>";

    	foreach ($_SESSION['cart'] as $key => $value) {
    		
    		$table .= "<tr>";
	    		$table .= "<td>".$value['name']."</td>";
				$table .= "<td>".$value['amount']."</td>";
				$table .= "<td>".$value['quantity']."</td>";
				$table .= "<td>".$value['result']."</td>";
				$table .= "<td>
								<button class='btn btn-danger' onclick='remove_session_cart(".$key.");'>
									X
								</button>
							</td>";
			$table .= "</tr>";
    	}

		$table .= "</table>";

		$result = count($_SESSION['cart']);

		if( $result > 0 ){
			$table = $table;
		}else{
			$table = "Data not found";
		}

		$table .= "<span class='count_refresh' style='display:none;'>".$result."</span>";
		echo $table;

    	break;
    case "removecart":

    	# Load up session
		session_start();

		# Field parameter get
    	$param = $_GET;

    	# Value key array
    	$key = $param['id'];

    	# Destroy unset key session
    	unset($_SESSION['cart'][$key]);

    	$count = count($_SESSION['cart']);

    	echo json_encode(array( 'count'=> $count ));


    	break;
    case "pay":

    	# Load up session
		session_start();
		session_regenerate_id(true);

    	# Param field post
    	$param = $_POST;

        // Get field post shipping
        $shipping = $param['shipping'];

        // Variable session
        $email = $_SESSION['email'];
        // Call method getUsers
        $getUsers = CUsers::getUsers( $email );

        // User id
        $users_id = $getUsers[0];
        // The user's available balance is captured
        $get_disponibility = CUsers::usersDisponibility( $users_id );
        $user_amount = $get_disponibility[0];

        // Each data cart list products of save the db

        // Field default in 0
        $sum_total_cost        = 0;
        $sum_remaining_balance = 0;
		foreach ($_SESSION['cart'] as $key => $value) {
			
			$product_id = $value['product_id'];
            $quantity   = $value['quantity'];
            $amount     = $value['amount'];
            $total_cost = $value['result'];

            // Array values for the send to cart
            $data_one = array(
                'users_id' => $users_id,
                'product_id' => $product_id,
                'quantity' => $quantity,
                'amount' => $amount,
                'total_cost' => $total_cost,
                'shipping' => $shipping
            );

            // Add to cart
            $addcart = CUsers::addCart( $data_one );

            if($addcart){
                $sum_total_cost += $total_cost; 
                $sum_remaining_balance = (float)$user_amount - (float)$sum_total_cost ;

            }

		}

        // Array values for the send to cart
        $data_two = array(
            'user_id' => $users_id,
            'previous_balance' => $user_amount,
            'total_cost' => $sum_total_cost,
            'remaining_balance' => $sum_remaining_balance
        );

        // Add balance to user
        $add_balance = CUsers::addBalance( $data_two );

        // Update set value amount
        $add_balance = CUsers::updateDisponibility( $users_id, $sum_remaining_balance );

        # Load up session
        session_start();

        # Destroy unset key session
        unset($_SESSION['cart']);

        echo 1;


    	break;
    case "users_disponibility":

        # Load up session
        session_start();

        // Variable session
        $email = $_SESSION['email'];
        // Call method getUsers
        $getUsers = CUsers::getUsers( $email );

        // User id
        $users_id = $getUsers[0];

        // The user's available balance is captured
        $get_disponibility = CUsers::usersDisponibility( $users_id );
        $user_amount = str_ireplace("-", "", $get_disponibility[0]);

        echo "Disponibility: ".$user_amount."$";

        break;
    case "history_balance":

        # Load up session
        session_start();

        // Variable session
        $email = $_SESSION['email'];
        // Call method getUsers
        $getUsers = CUsers::getUsers( $email );

        // User id
        $users_id = $getUsers[0];
        
        $history_balance = CUsers::historyBalance( $users_id );

        $data['previous_balance']  = $history_balance['previous_balance'];
        $data['total_cost']        = $history_balance['total_cost'];
        $data['remaining_balance'] = $history_balance['remaining_balance'];
        
        echo json_encode(array($data));

        break;
    case "puntuations_star":

        # Load up session
        session_start();

        // Variable session
        $email = $_SESSION['email'];
        // Call method getUsers
        $getUsers = CUsers::getUsers( $email );

        // User id
        $users_id = $getUsers[0];

        # Param field post
        $param = $_POST;
        $param = explode(";", $param['star']);

        $product_id = $param[0];
        $star       = $param[1];

        $data = array(
            'user_id' => $users_id,
            'product_id' => $product_id,
            'star' => $star
        );

        return CUsers::addPuntuation( $data );
        

        break;
    case "average":

        $average = CUsers::previewAverage();

        $content = "<table class='table' border=1 style='width:100%;'>
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Cant users</th>
                                <th>Cant star</th>
                                <th>Average</th>
                            </tr>
                        </thead>
                        <tbody>";

                        foreach ($average as $key => $value) {
                            
                            $content .= "<tr>
                                            <td>".$value['name']."</td>
                                            <td>".$value['cant_users']."</td>
                                            <td>".$value['sum_star']."</td>
                                            <td>".$value['average']."</td>
                                        </tr>";

                        }
                            
            $content .= "</tbody>";
        $content .= "</table>";

        echo $content;

        break;
}
?>