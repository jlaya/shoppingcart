<?php 

	/**
	 * 
	 */
	class CProducts
	{	
		# Variable private
		private $model;
		
		function __construct()
		{
			//Call for models
			require_once("models/MProducts.php");
			# Instance Model Products
			$this->model = new MProducts();

		}

		public function listProducts(){
			
			$obj = $this->model->listProducts();
			return $obj;
		}
	}

 ?>