<?php 
	
	// Global values sistem
	require_once("../global/global.php");

	/**
	 * 
	 */
	class CUsers
	{

		// Method for sign up users
		public function login( $email , $password ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$obj = $model->login( $email , $password );
			return $obj;
		}

		// Method for sign up users
		public function getUsers( $email ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$obj = $model->getUsers( $email );
			return $obj;
		}

		// Method for users disponibility
		public function usersDisponibility( $users_id ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$obj = $model->usersDisponibility( $users_id );
			return $obj;
		}

		// Method for add cart users
		public function addCart( $data ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->addCart( $data );
			return $save;
		}

		// Add balance to user
		public function addBalance( $data ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->addBalance( $data );
			return $save;
		}

		// Update set value amount
		public function updateDisponibility( $users_id , $remaining_balance ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->updateDisponibility( $users_id , $remaining_balance );
			return $save;
		}

		// Get history_balance
		public function historyBalance( $user_id ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->historyBalance( $user_id );
			return $save;
		}

		// Add puntuations
		public function addPuntuation( $data ){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->addPuntuation( $data );
			return $save;
		}

		// Preview for average
		public function previewAverage(){

			//Call for models
			require_once(base_url."/models/MUsers.php");

			# Instance Model MUsers
			$model = new MUsers();

			$save = $model->previewAverage();
			return $save;
		}
	}

 ?>