<?php 

	# Conection for DB
	require_once("conn/index.php");
	
	/**
	 * Class Model Products
	 */
	class MProducts
	{
		
		function __construct()
		{
			$obj = new Conn();
			# Call method conection DB
			$this->db = $obj->conn();
			# Add array the list products
			$this->products = array();
		}

		# Method for list products and return array objects
		public function listProducts(){
			$result = $this->db->query("select * from products ORDER BY name ASC");
			while( $row = $result->fetch_assoc()){
	            $this->products[] = $row;
	        }
	        return $this->products;

		}
	}

 ?>