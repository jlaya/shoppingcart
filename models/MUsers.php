<?php 
	
	// Global values sistem
	require_once("../global/global.php");

	# Conection for DB
	require_once(base_url."/conn/index.php");
	
	/**
	 * Class Model MUsers
	 */
	class MUsers
	{
		
		function __construct()
		{
			$obj = new Conn();
			# Call method conection DB
			$this->db = $obj->conn();
			# Add array the list puntuations
			$this->puntuations = array();
		}

		# Method for list products and return array objects
		public function login( $email , $password ){
			$result = $this->db->query("select * from users WHERE email = '$email' AND password = '$password'");
			return $result->num_rows;
		}

		# Method for list products and return array objects
		public function getUsers( $email ){
			$result = $this->db->query("select id from users WHERE email = '$email'");
			return $result->fetch_row();
		}

		// Method for users disponibility
		public function usersDisponibility( $users_id ){
			$result = $this->db->query("select amount from users_disponibility WHERE users_id = '$users_id'");
			return $result->fetch_row();
		}

		// Method for add cart users
		public function addCart( $data ){

			$query = $this->db->query("
				INSERT INTO cart (user_id, product_id, quantity, amount, total_cost, shipping)
				 VALUES (
					 ".$data['users_id'].", 
					 ".$data['product_id'].", 
					 ".$data['quantity'].", 
					 ".$data['amount'].", 
					 ".$data['total_cost'].",
					 '".$data['shipping']."'
					 )
				 ");

			if ($query) {
               return 'ok';
            }

		}

		// Method for add balance to user
		public function addBalance( $data ){

			$query = $this->db->query("
				INSERT INTO history_balance (user_id, previous_balance, total_cost, remaining_balance)
				 VALUES (
					 ".$data['user_id'].", 
					 ".$data['previous_balance'].", 
					 ".$data['total_cost'].", 
					 ".$data['remaining_balance']."
					 )
				 ");

			if ($query) {
               return 'ok';
            }

		}

		// Method for update set value amount
		public function updateDisponibility( $users_id , $remaining_balance ){

			$query = $this->db->query("UPDATE users_disponibility SET amount = $remaining_balance WHERE users_id = $users_id");

			if ($query) {
               return 'ok';
            }

		}

		// Get history_balance
		public function historyBalance( $user_id ){
			$result = $this->db->query("SELECT * FROM history_balance WHERE user_id = $user_id order by id desc LIMIT 1");
			return $result->fetch_array();

		}

		// Method for Add puntuations
		public function addPuntuation( $data ){

			$result = $this->db->query("select * from puntuations WHERE user_id = ".$data['user_id']." AND product_id = ".$data['product_id']." ");
			$exists = $result->num_rows;

			if($exists > 0 ){
				echo 'Update puntuation';
				$query = $this->db->query("UPDATE puntuations SET star = ".$data['star']." WHERE user_id = ".$data['user_id']." AND product_id = ".$data['product_id']." ");
			}else{

				$query = $this->db->query("
					INSERT INTO puntuations (user_id, product_id, star)
					 VALUES (
						 ".$data['user_id'].", 
						 ".$data['product_id'].", 
						 ".$data['star']."
						 )
					 ");

				if ($query) {
	               echo 'Sending puntuation';
	            }

			}


		}

		// Method for preview for average
		public function previewAverage(){
			$result = $this->db->query("SELECT a.name, COUNT(b.user_id) AS cant_users, SUM(b.star) AS sum_star, COUNT(b.user_id) / SUM(b.star) AS average
				FROM products AS a
				INNER JOIN puntuations AS b ON(b.product_id=a.id)
				GROUP BY a.name");
			while( $row = $result->fetch_assoc()){
	            $this->puntuations[] = $row;
	        }
	        return $this->puntuations;
		}


	}

 ?>